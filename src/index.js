const express = require("express");

const PORT = 3000;

const app = express();

app.get("/", (req, res) => {
  res.status(200).json({
    status: "success",
    message: "Hello World",
  });
});

app.get("/health", (req, res) => {
  res.status(200).json({
    status: "success",
    message: "system looks healthy and working fine",
  });
});

app.get("/system-date", (req, res) => {
  res.status(200).json({
    status: "success",
    systemDate: new Date().toISOString(),
  });
});

app.listen(PORT, () => {
  console.log("Listening server on port %s", PORT);
});
